# ft_retro

Space Invaders clone written in C++ using the SDL 2 library
And in python using the curses library

The python is much more developed than the C++ version for now

## Python verion:
There are no additional dependacies needed but if you're on windows you'll have to install the python curses library
```
https://www.lfd.uci.edu/~gohlke/pythonlibs/#curses
```

## To Run
```
python src/main.py
```

Space -> Shoot
Arrow keys -> Move
Enter -> AFK Mode

![](p_retro.png)

## C++ version:
Requires SDL2 libraries to compile and is an early version of the game,
since then a lot of changes have been made

![](c_retro.png)

# TODO
Animations
Another graphics display
Ai alien invaders game logic 
Shop
Sound
Upgrades
Move based on algo
Ship Builder

# Strech Goal
Add Moonbase Game
