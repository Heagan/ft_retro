#ifndef ACTOR_H
# define ACTOR_H

# include <vector>
# include <fstream>
# include <string>
# include <iostream>

# include "Location.class.hpp"

class Actor {

public:
	Actor( void );
	Actor( Actor const &actor );
	~Actor( void );
	Actor &operator=( Actor const &actor );
	
	void				loadModel(std::string file);

	Location			getMiddle();

	bool				collide( Location & l );
	char				destroyIfCollide( Location & obj );
	void				damagePart( Location & loc );

	char				model[50][50];
	Location			location;
	Location			locMiddle;
	int					lives;
	float				health;
	unsigned int		damage;
	unsigned int		score;
	unsigned int		value;

};

#endif