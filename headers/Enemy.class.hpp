#ifndef ENEMY_CLASS_H
# define ENEMY_CLASS_H
# include "game.h"
# include "Location.class.hpp"
# include "Actor.class.hpp"

class Enemy : public Actor {

public:
	Enemy( void );
	Enemy( Enemy const &act );
	~Enemy( void );
	Enemy &operator=( Enemy const &act );

};

#endif