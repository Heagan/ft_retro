#ifndef LOCATION_H
# define LOCATION_H

#include <cmath>

class Location {

public:
	Location( void );
	Location(unsigned int x, unsigned int y );
	Location( Location const &loc );
	~Location( void );
	Location &operator=( Location const &loc );
	bool operator==( Location const &loc );

	double	calcDistance( Location dst );
	void	setX(int x);
	void	setY(int y);
	int		getX( void );
	int		getY( void );
	int		getPX( void );
	int		getPY( void );
	void 	setLocation(int x, int y);
	void	append(int x, int y);
	void	append(int x, int y, int ***map, int type, Location worldBounds);

private:
	int x;
	int y;

	int px;
	int py;
};

#endif