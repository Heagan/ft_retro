#ifndef BULLET_H
# define BULLET_H

# include <ctime>
# include "Location.class.hpp"

enum eTarget {PLAYER, ENEMY, DEBRIE};

class Bullet {

public:
	Bullet( eTarget target, int x, int y, char icon = '-', double lifeDistance = 0 );
	Bullet( Bullet const &bull );
	~Bullet( void );
	Bullet &operator=( Bullet const &bull );

	void	setVelocity(int xv, int yv);
	void	appendVelocity(int xv, int yv);
	bool	checkLifeTime( void );

	Location	location;
	eTarget		target;
	double		lifeDistance;
	int			xv;
	int			yv;
	char		icon;

	Location	spawnLoc;

};

#endif