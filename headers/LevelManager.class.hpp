#ifndef LEVEL_MANAGER_H
# define LEVEL_MANAGER_H

class LevelManager {

public:
	LevelManager( void );
	LevelManager( LevelManager &object );
	~LevelManager( void );
	LevelManager &operator=( LevelManager const &object );

};

#endif
