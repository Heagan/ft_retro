#ifndef OBJECTS_H
# define OBJECTS_H

class Objects {

public:
	Objects( void );
	Objects( Objects &object );
	~Objects( void );
	Objects &operator=( Objects const &object );

};

#endif
