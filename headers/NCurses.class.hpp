#ifndef N_CURSES_H
# define N_CURSES_H

# include <iostream>
# include <vector>
# include <ncurses.h>
# include <string>
# include <cstring>
# include <unistd.h>

# include "Actor.class.hpp"
# include "Player.class.hpp"
# include "Enemy.class.hpp"
# include "Bullet.class.hpp"

enum	eKey {UP, DOWN, LEFT, RIGHT, QUIT, SPACE, NUL};

class NCurses {

public:
	NCurses( void );
	NCurses( NCurses const &object );
	~NCurses( void );
	NCurses &operator=( NCurses const &object );
	
	void	init( void );
	void	handleInput();

	void	drawBorders( void );
	void	drawStats( Player & p );
	void	displayMessage( std::string s, int x = 1, int y = 1 );
	void	drawModel(  Actor & actor );

	void	drawEnemies( std::vector<Enemy*> & enemies );
	void	drawBullets( std::vector<Bullet*> & bullets );
	void	drawDebrie( std::vector<Bullet*> & debrie );

	void 	draw_borders(WINDOW *screen);

	void	display( void );
    void	putpixel(unsigned int x, unsigned int y, char c);
    void	clearpixels( void ) ;
	void	endGame( void );

	int		screenWidth;
	int		screenHeight;
	int		key;
	
	bool	space;

private:
	WINDOW * _displaywin;
	WINDOW * _gamewin;

};

#endif
