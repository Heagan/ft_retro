#ifndef GAME_CLASS_H
# define GAME_CLASS_H

# include <vector>
# include <list>

# include "Map.class.hpp"
# include "Enemy.class.hpp"
# include "Player.class.hpp"
# include "NCurses.class.hpp"

class Game {

public:
	Game( void );
	Game(Game const & game);
	~Game( void );
	Game &operator=( Game const &game );

	void	handleInput( void );

	void	updateEnemies( void );
	void	updateBullets( void );
	void	updateDebries( void );
	void	updatePlayer( void );
	
	void	destroyBullet(int index);
	void	destroyDebrie(int index);
	void	destroyEnemy(int index);

	void	explodeActor( Actor & a );

	void	spawnDebris( Location l, char icon = 'X', int xv = 0, int yv = 0 );
	void	spawnBullet( Location l );
	void	spawnEnemy( Location l );

	Player	player;
	Player	playerAi;
	std::vector<Enemy*>		enemies;
	std::vector<Bullet *>	bullets;
	std::vector<Bullet *>	debries;

	NCurses		n;

	int			***map;	
	// Map Key:
	// 0 - player
	// 1 - Enemy
	// 2 - bullets
	// 3 - debrie


};

#endif