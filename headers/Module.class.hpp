#ifndef MODULE_H
# define MODULE_H

class Module {

public:
	Module( void );
	Module( Module &object );
	~Module( void );
	Module &operator=( Module const &object );

};

#endif
