#ifndef MAP_CLASS_H
# define MAP_CLASS_H

#include <strings.h>

class MapController {

public:
	MapController( void );
	MapController( unsigned int w, unsigned int h );
	MapController( MapController const &map );
	~MapController( void );
	MapController &operator=( MapController const &map );

	

};

#endif