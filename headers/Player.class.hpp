#ifndef PLAYER_CLASS_H
# define PLAYER_CLASS_H
# include "game.h"
# include "Actor.class.hpp"
# include "Location.class.hpp"

class Player : public Actor {

public:
	Player( void );
	Player( Player const &act );
	~Player( void );
	Player &operator=( Player const &act );

};

#endif