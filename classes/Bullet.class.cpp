# include "Bullet.class.hpp"
# include <stdlib.h>

Bullet::Bullet( eTarget target, int x, int y, char icon, double lifeDistance ) : target(target), icon(icon), lifeDistance(lifeDistance) {
	this->spawnLoc.setLocation(x, y);
	this->location.setLocation(x, y);
	switch (target) {
		case (PLAYER) : {
			xv = 1;
			yv = 0;
			break ;
		}
		case (ENEMY) : {
			xv = -1;
			yv = 0;
			break ;
		}
		case (DEBRIE) : {
			xv = 1;
			yv = rand() % 2 ? 1 : -1;
			break ;
		}
	}
}

Bullet::Bullet( Bullet const &bullet ) {
	*this = bullet;
}

Bullet::~Bullet( void ) {

}

Bullet &Bullet::operator=( Bullet const &bullet ) {
	return *this;
}

void	Bullet::setVelocity(int xv, int yv) {
	this->xv = xv;
	this->yv = yv;
}

void	Bullet::appendVelocity(int xv, int yv) {
	this->xv += xv;
	this->yv += yv;
}

bool	Bullet::checkLifeTime() {
	if (location.calcDistance(spawnLoc) > lifeDistance) {
		return true;
	}
	return false;
}
