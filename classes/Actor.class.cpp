# include "Actor.class.hpp"

Actor::Actor( void ) : score(0), lives(3) {
	this->location = Location(0, 0);
	this->locMiddle = getMiddle();
}


Actor::Actor( Actor const &act ) {
	*this = act;
}

Actor::~Actor( void ) {

}

Actor	&Actor::operator=( Actor const &act ) {

	return *this;
}


Location Actor::getMiddle( ) {
	int x, y = 0;

	while(model[y][0]) {
		x = 0;
		while (model[y][x]) {
			if (model[y][x] == 'H') {
				return Location(x, y);
			}
			x++;
		}
		y++;
	}
	return Location (x / 2, y / 2);
}

void	Actor::loadModel(std::string file) {
	std::ifstream	f;
	std::string		line;

	f.open(file);
	if (!f) {
		std::cout << "Can't open file!\n";
		throw "Can't open file";
	}

	std::getline(f, line);
	this->health = atoi(line.c_str());
	std::getline(f, line);
	this->damage = atoi(line.c_str());
	std::getline(f, line);
	this->value = atoi(line.c_str());
	
	int y = 0;
	while (std::getline(f, line)) {
		for (int i = 0; i < line.length(); i++) {
			this->model[y][i] = line[i];
			this->model[y][i + 1] = '\0';
		}
		this->model[y + 1][0] = '\0';
		y++;
	}

	f.close();
}

void			Actor::damagePart( Location & loc ) {
	int x, y =0;

	while (this->model[y][0]) {
		x = 0;
		while (this->model[y][x]) {
			if (x == loc.getX()) {
				if (y == loc.getY()) {
					this->model[y][x] = ' ';
				}
			}
			x++;
		}
		y++;
	}
}

bool				Actor::collide( Location & l ) {
	int x, y = 0;

	while (this->model[y][0]) {
		x = 0;
		while (this->model[y][x]) {
			if (x - getMiddle().getX() + location.getX() == l.getX()) {
				if (y - getMiddle().getY() + location.getY() == l.getY()) {
					if (this->model[y][x] != ' ')
						return true;
				}
			}
			x++;
		}
		y++;
	}
	return false;
}

char		Actor::destroyIfCollide( Location & loc ) {
	char	icon;
	int x, y = 0;

	while (this->model[y][0]) {
		x = 0;
		while (this->model[y][x]) {
			if (x - getMiddle().getX() + location.getX() == loc.getX()) {
				if (y - getMiddle().getY() + location.getY() == loc.getY()) {
					icon = this->model[y][x]; 
					if (this->model[y][x] != ' ')
						this->model[y][x] = ' ';
						return icon;
				}
			}
			x++;
		}
		y++;
	}
	return ' ';
}