# include "Game.class.hpp"

# define MAP_SIZE 1000

Game::Game(void) {
	map = (int ***)malloc(sizeof(int) * 4);
	for (int z = 0; z < 4; z++ ) {
		map[z] = (int **)malloc(sizeof(int) * MAP_SIZE);
		for (int y = 0; y < MAP_SIZE; y++ ) {
			map[z][y] = (int *)malloc(sizeof(int) * MAP_SIZE);
			for (int x = 0; x < MAP_SIZE; x++ ) {
				map[z][y][x] = 0;
			}
		}
	}
	map[0][player.location.getY()][player.location.getX()] = 1;
	map[0][playerAi.location.getY()][playerAi.location.getX()] = 1;
}

Game::Game(Game const &game) {
	*this = game;
}

Game::~Game(void) {
}

Game &Game::operator=(Game const &game) {
	return *this;
}

std::string	getEnemyFileName() {
	int	chance = rand() % 100;

	if (chance > 99) {
		return "models/the_cube.mod";
	} else if (chance > 90) {
		return "models/enemy_big.mod";
	} else if (chance > 80) {
		return "models/cruser.mod";
	} else if (chance > 70) {
		return "models/ufo.mod";
	} else if (chance > 60) {
		return "models/bi.mod";
	} else if (chance > 50) {
		return "models/enemy_med.mod";
	} else {
		return "models/enemy_small.mod";
	}
}

void	Game::handleInput() {
	int	key = n.key;

	switch (key) {
		case(UP): {
			this->player.location.append(0, -1, map, 0, Location(n.screenWidth, n.screenHeight));
			break;
		}
		case(DOWN): {
			this->player.location.append(0, 1, map, 0, Location(n.screenWidth, n.screenHeight));
			break;
		}
		case(LEFT): {
			this->player.location.append(-1, 0, map, 0, Location(n.screenWidth, n.screenHeight));
			break;
		}
		case(RIGHT): {
			this->player.location.append(1, 0, map, 0, Location(n.screenWidth, n.screenHeight));
			break;
		}
		case(QUIT): {
			exit(0);
			break;
		}
		case(SPACE): {
			spawnBullet(this->player.location);
			break;
		}
	}
	if (n.space) {
		spawnBullet(this->player.location);
	}
	//spawnBullet(this->playerAi.location);
	n.key = -1;
}

void	Game::spawnDebris( Location l, char icon, int xv, int yv ) {
	Bullet *d = new Bullet( DEBRIE, l.getX(), l.getY(), icon, 10);

	d->xv = xv;
	d->yv = yv;
	this->debries.push_back(d);
	map[3][l.getY()][l.getX()] = 1;
}

void	Game::spawnBullet( Location l ) {
	Bullet *b = new Bullet( PLAYER, l.getX(), l.getY());

	this->bullets.push_back(b);
//	std::cout << l.getX() << " " << l.getY() << std::endl;
	map[2][l.getY()][l.getX()] = 1;
//	std::cout << l.getX() << " " << l.getY() << std::endl;
}

void	Game::spawnEnemy( Location l ) {
	Enemy *e = new Enemy;
	e->loadModel(getEnemyFileName());
	e->location = l;
	enemies.push_back(e);
	map[1][l.getY()][l.getX()] = 1;
}

void	Game::explodeActor( Actor & a ) {
	int 		x, y = 0;
	int 		xv, yv = 0;
	Location	sp = a.location;

	player.score += a.value;
	while (a.model[y][0]) {
		x = 0;
		while (a.model[y][x]) {
			if (a.model[y][x] == ' ') {
				x++;
				continue ;
			}
			xv = x - a.getMiddle().getX();
			yv = y - a.getMiddle().getY();
			if (xv == 0 && yv == 0) {
				xv = 1;
			}
			spawnDebris(a.location, a.model[y][x], xv, yv);
			x++;
		}
		y++;
	}
}


void	Game::updateEnemies( ) {
	while (this->enemies.size() < 10) {
		int x = n.screenWidth - (rand() % 50);
		int y = rand() % (n.screenHeight - 2) + 1;
		spawnEnemy(Location(x, y));
	}

	for (int i = 0; i < this->enemies.size(); i++) {
		Enemy &enemy = *this->enemies.at(i);
		if ((enemy.location.getX() < 5 || enemy.location.getX() > n.screenWidth) || (enemy.location.getY() < 0 || enemy.location.getY() > n.screenHeight)) {
			destroyEnemy(i);
		}
		map[1][enemy.location.getY()][enemy.location.getX()] = 0;
		enemy.location.append(-1, 0);
		map[1][enemy.location.getY()][enemy.location.getX()] = 1;
	}
}

void	Game::updateBullets( ) {
	char icon;

	for (int i = 0; i < this->bullets.size(); i++) {
		Bullet &b = *this->bullets.at(i);
		if ((b.location.getX() < 0 || b.location.getX() > n.screenWidth) || (b.location.getY() < 0 || b.location.getY() > n.screenHeight)) {
			destroyBullet(i);
			return ;
		}

		map[2][b.location.getY()][b.location.getX()] = 0;
		b.location.append(b.xv, b.yv);
		map[2][b.location.getY()][b.location.getX()] = 1;

		for (int enemyIndex = 0; enemyIndex < enemies.size(); enemyIndex++ ) {
			Actor & a = *enemies.at(enemyIndex);

			if ((icon = enemies.at(enemyIndex)->destroyIfCollide(b.location)) != ' ') {
				int xv = rand() % 2 ? 1 : rand() % 2 ? -1 : 0;
				int yv = rand() % 2 ? 1 : -1;

				spawnDebris(a.location, icon, xv, yv);
				if (i > this->bullets.size() - 1)
					return ;
				destroyBullet(i);
				if (icon == 'H') {
					explodeActor(a);
					destroyEnemy(enemyIndex);
				}
			}
		}
	}
}

void	Game::updateDebries( ) {
	char icon;

	for (int i = 0; i < debries.size(); i++) {
		Bullet &d = *debries.at(i);
		if (d.checkLifeTime() || (d.location.getX() < 0 || d.location.getX() > n.screenWidth) || (d.location.getY() < 0 || d.location.getY() > n.screenHeight)) {
			destroyDebrie(i);
		}
		map[3][d.location.getY()][d.location.getX()] = 0;
		d.location.append(d.xv, d.yv);
		map[3][d.location.getY()][d.location.getX()] = 1;
	}
}

void	Game::updatePlayer( ) {
	for (int i = 0; i < this->enemies.size(); i++) {
		Enemy &enemy = *this->enemies.at(i);
		if (enemy.collide(player.location)) {
			player.health -= 1;
		}
	}

	if (player.health <= 0) {
		player.lives--;
		player.health = 100;
	}
	if (player.lives < 0) {
		n.endGame();
	}

	if (enemies.size() > 0) {
		if (enemies.at(0)->location.getX() - playerAi.location.getX() > 5) {
			int y = 0;
			if (enemies.at(0)->location.getY() != playerAi.location.getY()) {
				y = enemies.at(0)->location.getY() > playerAi.location.getY() ? 1 : -1;
			}
			map[0][playerAi.location.getY()][playerAi.location.getX()] = 0;
			playerAi.location.append(0, y);
			map[0][playerAi.location.getY()][playerAi.location.getX()] = 1;
		}
	}
}

void	Game::destroyBullet(int index) {
	Location l = bullets.at(index)->location;
	map[2][l.getY()][l.getX()] = 0;
	
	bullets.erase(bullets.begin() + index);
}

void	Game::destroyDebrie(int index) {
	Location l = debries.at(index)->location;
	map[3][l.getY()][l.getX()] = 0;
	
	debries.erase(debries.begin() + index);
}

void	Game::destroyEnemy(int index) {
	Location l = enemies.at(index)->location;
	map[2][l.getY()][l.getX()] = 0;

	enemies.erase(enemies.begin() + index);
}



