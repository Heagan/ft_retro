# include "Location.class.hpp"

Location::Location( void ) : x(0), y(0), px(0), py(0) {

}

Location::Location(unsigned int x, unsigned int y ) : x(x), y(y), px(0), py(0) {

}


Location::Location( Location const &loc ) {
	*this = loc;
}

Location::~Location( void ) {

}

Location &Location::operator=( Location const &loc ) {
	this->px = loc.px;
	this->py = loc.py;
	this->x = loc.x;
	this->y = loc.y;
	return *this;
}

bool Location::operator==( Location const &loc ) {
	if ((this->x == loc.x) && (this->y == loc.y))
		return true;
	return false;
}

void	Location::append(int x, int y) {
	this->px = this->x;
	this->py = this->y;
	this->x += x;
	this->y += y;
}

void	Location::append(int x, int y, int ***map, int type, Location worldBounds) {
	if (((this->x >= 0 + x) && (this->x < worldBounds.getX() - x)) && ((this->y >= 0 + y) && (this->y < worldBounds.getY() - y))) {
		map[type][this->y][this->y] = 0;
		append(x, y);
		map[type][this->y][this->y] = 1;
	} else {
		append(x, y);
	}
}

void	Location::setX(int x) {
	this->px = this->x;
	this->x = x;
}

void	Location::setY(int y) {
	this->py = this->y;
	this->y = y;
}

void 	Location::setLocation(int x, int y) {
	this->setX(x);
	this->setY(y);
}

int		Location::getX( void ) {
	return this->x;
}

int		Location::getY( void ) {
	return this->y;
}


int		Location::getPX( void ) {
	return this->px;
}

int		Location::getPY( void ) {
	return this->py;
}

double	sqr(double n) {
	return n * n;
}

double	Location::calcDistance( Location dst ) {
	return std::sqrt(sqr(this->x - dst.x) + sqr(this->y - dst.y));
}