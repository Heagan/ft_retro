#include "NCurses.class.hpp"
#include <curses.h>
#include <stdio.h>
#include <string.h>

void NCurses::init( void ) {
	srand(time(NULL));

	if (!initscr()) {
		throw "Failed to init NCurses!";
	}
	noecho();
	curs_set(0);

	if (!(this->_displaywin = newwin(LINES, COLS, 0, 0))) {
		throw "Failed to create NCurses window!";
	}

	if (!(this->_gamewin = newwin(((float)LINES / 10) * 9 - 1, COLS - 2, ((float)LINES / 10) + 1, 1))) {
		throw "Failed to create NCurses window!";
	}

	refresh();
	this->screenWidth = COLS;
	this->screenHeight = LINES;


	keypad(this->_gamewin,TRUE);
}

NCurses::NCurses( void ) {
	this->init();
}

NCurses::NCurses( NCurses const &object ) {
	*this = object;
}

NCurses::~NCurses( void ) {
	delwin(this->_gamewin);
	delwin(this->_displaywin);
	curs_set(1);
	std::cout << "NCURSES DESTROYED!!!!\n";
}

char const *getCharFromColour(int colour) {
	char c[2];

	c[0] = ((colour % 26) + 65);
	c[1] = '\0';
	
	return std::string(c).c_str();
}

NCurses	&NCurses::operator=( NCurses const &object ) {
	return *this;
}

		
void NCurses::handleInput( void ) {
	wtimeout(_gamewin, 0);
	int input = wgetch(_gamewin);

	switch (input) {
		case KEY_RIGHT:
			this->key = RIGHT;
			break;
		case KEY_LEFT: 
			this->key = LEFT;
			break;
		case KEY_UP: 
			this->key = UP;
			break;
		case KEY_DOWN: 
			this->key = DOWN;
			break;
		case 27:
			this->key = QUIT;
			break;
		case 32:
			this->space = this->space ? 0 : 1;
			break;
		default:
			break;
	}
}

void	NCurses::drawModel( Actor & actor) {
	int x, y = 0;

	while(actor.model[y][0]) {
		x = 0;
		while (actor.model[y][x]) {
			putpixel(actor.location.getX() - actor.getMiddle().getX() + x,  actor.location.getY() - actor.getMiddle().getY() + y, actor.model[y][x]);
			x++;
		}
		y++;
	}
}

void	NCurses::drawBorders() {
	wresize(_displaywin, LINES, COLS);
	box(_displaywin, (int)'|', (int)'-');
	mvwhline(_displaywin, LINES / 10, 1, '-', COLS - 2);
	mvwvline(_displaywin, 1, COLS / 3, '|', (LINES / 10) - 1);
	mvwvline(_displaywin, 1, COLS / 3 + COLS / 3, '|', (LINES / 10 - 1));
}

void	NCurses::displayMessage( std::string s, int x, int y ) {
	mvwprintw(_displaywin, x, y, s.c_str());
}

void	NCurses::drawStats( Player & p ) {
	mvwprintw(_displaywin, 2, (COLS / 9 - 1) * 1, std::string("Lives: " + std::to_string(p.lives)).c_str());
	mvwprintw(_displaywin, 2, (COLS / 9 - 1) * 4, std::string("Health: " + std::to_string((int)p.health)).c_str());
	mvwprintw(_displaywin, 2, (COLS / 9 - 1) * 7, std::string("Score: " + std::to_string(p.score)).c_str());
}

void	NCurses::display( void ) {
	wrefresh(_displaywin);
	draw_borders(_gamewin);
	handleInput();
	wrefresh(_gamewin);
}

void	NCurses::drawEnemies( std::vector<Enemy*> & enemies ) {
	for (int i = 0; i < enemies.size(); i++) {
		drawModel(*enemies.at(i));
	}
}

void	NCurses::drawBullets( std::vector<Bullet*> & bullets ) {
	for (int i = 0; i < bullets.size(); i++) {
		putpixel(bullets.at(i)->location.getPX(), bullets.at(i)->location.getPY(), ' ');
		putpixel(bullets.at(i)->location.getX(), bullets.at(i)->location.getY(), bullets.at(i)->icon);
	}
}

void	NCurses::drawDebrie( std::vector<Bullet*> & debrie ) {
	for (int i = 0; i < debrie.size(); i++) {
		putpixel(debrie.at(i)->location.getPX(), debrie.at(i)->location.getPY(), ' ');
		putpixel(debrie.at(i)->location.getX(), debrie.at(i)->location.getY(), debrie.at(i)->icon);
	}
}

void	NCurses::putpixel(unsigned int x, unsigned int y, char c) {
	mvwaddch(_gamewin, y, x, c);
}

void	NCurses::clearpixels( void ) {
	nodelay(stdscr, TRUE);
	nodelay(this->_gamewin, TRUE);
	wclear(this->_gamewin);
}

void	NCurses::endGame( void ) {
	std::cout << "\nGAME OVER!\n";
	exit(0);
}

void NCurses::draw_borders(WINDOW *screen) {
  int x, y, i;

  getmaxyx(screen, y, x);

  // 4 corners
  mvwprintw(screen, 0, 0, "+");
  mvwprintw(screen, y - 1, 0, "+");
  mvwprintw(screen, 0, x - 1, "+");
  mvwprintw(screen, y - 1, x - 1, "+");

  // sides
  for (i = 1; i < (y - 1); i++) {
    mvwprintw(screen, i, 0, "|");
    mvwprintw(screen, i, x - 1, "|");
  }

  // top and bottom
  for (i = 1; i < (x - 1); i++) {
    mvwprintw(screen, 0, i, "-");
    mvwprintw(screen, y - 1, i, "-");
  }
}
