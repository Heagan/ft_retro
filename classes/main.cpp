#include "game.h"
#include "NCurses.class.hpp"
#include "Game.class.hpp"
#include "Actor.class.hpp"

//SLEEP
#include <chrono>
#include <thread>

int  	FRAMES_PER_SECOND = 30;

void	update(Game * g) {
	return ;
	std::thread first(&Game::updateEnemies, g);
	std::thread second(&Game::updateBullets, g);
	std::thread third(&Game::updateDebries, g);
	std::thread forth(&Game::updatePlayer, g);
	first.join();
	second.join();
	third.join();
	forth.join();
}


void	render(Game * g) {
	g->n.clearpixels();
	g->n.drawModel(  g->player);
	g->n.drawModel(  g->playerAi);
	g->n.drawEnemies(g->enemies);
	g->n.drawBullets(g->bullets);
	g->n.drawDebrie( g->debries);
	g->n.drawStats(  g->player);
	g->n.display();
}

void	processInput(Game * g) {
	g->handleInput();
}

int main(int ac, char **av) {
	Game	g;

	g.n.drawBorders();

	g.player.loadModel("models/player.mod");
	g.player.location = Location(5, g.n.screenHeight / 2);

	g.playerAi.loadModel("models/player.mod");
	g.playerAi.location = Location(5, g.n.screenHeight / 2);

	while (1) {
		std::thread	first(processInput, &g);
		std::thread	second(update, &g);
		std::thread	third(render, &g);

		first.join();
		second.join();
		third.join();

		std::this_thread::sleep_for(
			std::chrono::milliseconds(((int)(1000 / FRAMES_PER_SECOND))));
	}

	return 0;
}