
Feature: Damage actor

  Scenario: When an Actor takes damage and dies
    Given an actor and n amount of damage
    When Health is lower than the damage
    Then Health needs to be set to 0
    And alive is set to false

  Scenario: When an Actor takes damage and lives
    Given an actor and n amount of damage
    When Health is above or equal to the damage
    Then Health needs to decrease by n amount of damage
    And alive is set to true

