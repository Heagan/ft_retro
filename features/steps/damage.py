from behave import *

from actor import Actor


@given('an actor and n amount of damage')
def step_impl(context):
    location = (0, 0)
    model = "models/player.mod"
    damage = 1

    context.actor = Actor(location, model)
    context.damage = damage


@when('Health is lower than the damage')
def step_impl(context):
    assert context.actor.health < context.damage


@then(u'Health needs to be set to 0')
def step_impl(context):
    context.actor.take_damage(context.damage)
    assert context.actor.health is 0


@then(u'alive is set to false')
def step_impl(context):
    assert context.actor.alive is False


@when(u'Health is above or equal to the damage')
def step_impl(context):
    assert context.actor.health >= context.damage


@then('Health needs to decrease by n amount of damage')
def step_impl(context):
    health_before_damage = context.actor.health
    context.actor.take_damage(context.damage)
    assert context.actor.health is health_before_damage - context.damage


@then('alive is set to true')
def step_impl(context):
    assert context.actor.alive is True
