from time import time
from random import randint
from copy import deepcopy
from misc import load_model_from_file, get_model_size

from logging import warning, basicConfig, DEBUG, log, error


class Actor:

    def __init__(self, location, model, xv=0, yv=0):
        self.t = 0
        self.move_t = 0
        
        self.model_buffer = load_model_from_file(model)
        self.size = get_model_size(self.model_buffer)
        self.model = load_model_from_file(model)

        self.health = int(self.size[0] * self.size[1]) / 2
        self.middle = (int(self.size[0] / 2), int(self.size[1] / 2))
        
        self.location = location
        self.xv = xv
        self.yv = yv
        
        self.alive = True
        self.is_damaged = False
        self.is_spawning = False


    def take_damage(self, location=(0, 0), damage: float=1, damage_only=False):
        if self.is_spawning:
            return

        self.health -= damage
        self.is_damaged = True

        if self.health < (self.size[0] * self.size[1]) / 4:
            self.alive = False
        if damage_only:
            return
            
        hx = int(location[0] - self.location[0])
        hy = int(location[1] - self.location[1])
        
        destroy_char = f"{self.model[hy][hx]}"
        self.model[hy][hx] = ' '
        return destroy_char, (hx, hy)


    def get_coords(self):
        coords = []
        for y, _ in enumerate(self.model):
            for x, _ in enumerate(self.model[y]):
                if not self.model[y][x] == ' ':
                    coords.append((self.location[0] + x, self.location[1] + y))
        return coords

    def move(self):
        passed = time() - self.move_t
        dx = self.xv * passed if passed <= 1 else 0
        dy = self.yv * passed if passed <= 1 else 0
        self.location = (self.location[0] + dx, self.location[1] + dy)
        self.move_t = self.move_t + passed

    def get_guns(self):
        guns = [
            '+',
            '>',
            # '<',
            # '='
        ]
        coords = []
        for y, _ in enumerate(self.model):
            for x, _ in enumerate(self.model[y]):
                if self.model[y][x] in guns:
                    coords.append((self.location[0] + x, self.location[1] + y))
        return coords

    def moveby(self, x, y):
        self.location = (self.location[0] + x, self.location[1] + y)
        pass

    def update(self):
        passed = time() - self.t
        if self.t % 1 > 0.5:
            self.is_damaged = False
        self.t = self.t + passed

        # if len(self.model_buffer) == 0:
        #     return
        # i = randint(0, len(self.model_buffer) - 1)
        # if i < 0:
        #     return
        # if len(self.model_buffer[i]) == 0:
        #     return
        # j = randint(1, len(self.model_buffer[i])) - 1
        # self.model[i][j] = self.model_buffer[i][j]
        # if str(self.model) == str(self.model_buffer):
        #     self.is_spawning = False
            