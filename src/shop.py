import misc
from misc import eEvents
from player import Player

from logging import warning, basicConfig, DEBUG, log, error

class Shop:

    def __init__(self, game):
        self.game = game
        misc.EVENT_MANAGER.subscribe("SHOP_INCREASE_LIFE", self.increase_life, eEvents["INCREASE_LIVES"])
        misc.EVENT_MANAGER.subscribe("SHOP_INCREASE_HEALTH", self.increase_health, eEvents["INCREASE_HEALTH"])

    def increase_life(self, **event_kwargs):
        warning(f"LIFE EVENT TRIGGERED!")
        amount = event_kwargs['args'] if 'args' in event_kwargs else 1
        self.game.player.lives += amount


    def increase_health(self, **event_kwargs):
        warning("HEALTH EVENT TRIGGERED!")
        amount = event_kwargs['args'] if 'args' in event_kwargs else 10
        self.game.player.health += amount

    def add_ai_player(self):
        self.game.player_ai = Player(location=(5, int(misc.SCREEN_HEIGHT / 2)), model="models/player.mod", lives=3, health=100)



