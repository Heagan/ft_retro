from time import time

from misc import eTarget


class Bullet:

    def __init__(self, target, location, icon='>', lifeDistance=0):
        self.spawn_location = location
        self.location = location
        self.icon = icon
        self.lifeDistance = lifeDistance
        self.target = target
        self.alive = True
        self.t = 0
        self.xv = 0
        self.yv = 0
        

    def move(self):
        passed = time() - self.t
        dx = self.xv * passed if passed <= 1 else 0
        dy = self.yv * passed if passed <= 1 else 0
        self.location = (self.location[0] + dx, self.location[1] + dy)
        self.t = self.t + passed

    def setVelocity(self, xv, yv):
        self.xv = xv
        self.yv = yv

    def get_coords(self):
        coords = []
        coords.append(self.location)
        coords.append((self.location[0] - 1, self.location[1]))
        coords.append((self.location[0] - 2, self.location[1]))
        # coords.append((self.location[0] + 1, self.location[1]))
        # coords.append((self.location[0] + 2, self.location[1]))
        return coords
