from actor import Actor
from time import time
from random import randint
from logging import warning, basicConfig, DEBUG, log, error

class Enemy(Actor):

    def __init__(self, location, model, score=500, xv=0, yv=0):
        Actor.__init__(self, location, model, xv=xv, yv=yv)
        self.score = score

    def move(self):
        super().move()

    def take_damage(self, location=(0, 0), damage: float=1, damage_only=False):
        return super().take_damage(location, 1, False)