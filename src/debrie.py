from bullet import Bullet
from misc import look_at_velocity
from random import randint


class Debrie(Bullet):

    def __init__(self, location, xv=0, yv=0, icon='*', lifeDistance=0, special = False):
        Bullet.__init__(self, "NONE", location, icon=icon,
                        lifeDistance=lifeDistance)
        self.target_vel = (xv, yv)
        self.setVelocity(xv, yv)
        self.special = special
        # if special:
        #     self.target_vel = look_at_velocity(self.location, (0, 0))
        #     self.target_vel = (-self.target_vel[0], -self.target_vel[1])

    def move(self):
        if self.special:
            if self.xv > self.target_vel[0] + 1:
                self.xv -= randint(1, 5)
            if self.xv < self.target_vel[0] - 1:
                self.xv += randint(1, 5)
            if self.yv > self.target_vel[1] + 1:
                self.yv -= randint(1, 5)
            if self.yv < self.target_vel[1] + -1:
                self.yv += randint(1, 5)
        super().move()

