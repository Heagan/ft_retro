from misc import eKey, eTarget, get_closest_actor, look_at_velocity, lround, get_model_heart_location
from copy import deepcopy
from actor import Actor
from player import Player
from enemy import Enemy
from bullet import Bullet
from debrie import Debrie
from base import Base
from shop import Shop
import misc

from os import path
from sys import exc_info
from logging import warning, basicConfig, DEBUG, log, exception, info, exception
from random import randint


class Map:
    world = {}

    def clear(self):
        self.world.clear()

    def add(self, location, target):
        if location[0] > misc.SCREEN_WIDTH or location[1] > misc.SCREEN_HEIGHT or \
            location[0] < 0 or location[1] < 0:
            return
        location = lround(location)
        self.world[location] = self.world[location] + \
            target if location in self.world else target

    def get(self, location):
        location = lround(location)
        return self.world[location] if location in self.world else ''


class Game:

    def __init__(self):
        log(level=DEBUG, msg="GAME INITLIZED")
        self.world = Map()
        self.shop = Shop(self)

        self.player = None
        self.player_ai = None
        self.base = Base((5, 5), 'models/square.mod')

        self.enemies = []
        self.bullets = []
        self.debrie = []

        self.afk = False
        self.target = None

        self.space = False
        self.count = 0
        self.import_once = True

        self.loaded_enemies = {}


    def handle_nput(self, nput):
        if not isinstance(nput, list):
            nput = [nput]
        for n in nput:
            if n == eKey["QUIT"]:
                exit(0)
            if n == eKey["UP"]:
                self.player.moveby(0, -3)
            elif n == eKey["DOWN"]:
                self.player.moveby(0, 3)
            elif n == eKey["LEFT"]:
                self.player.moveby(-2, 0)
            elif n == eKey["RIGHT"]:
                self.player.moveby(2, 0)
            # elif n == eKey["ENTER"]:
            #     self.afk = False if self.afk is True else True
            elif n == eKey["SPACE"]:
                gun_locations = self.player.get_guns()
                for location in gun_locations:
                    self.spawnBullet(location)

        if misc.MOUSE_ENABLED:
            self.player.location = (misc.MX, misc.MY - self.player.size[1] - self.player.middle[1])

        self.space = True if misc.MD == 2 else False if misc.MD == 1 else self.space
        if self.space:
            gun_locations = self.player.get_guns()
            for location in gun_locations:
                self.spawnBullet(location)

    def update(self):
        try:
            self.update_debrie()
            self.update_ai()
            self.update_player()
            self.update_enemies()
            self.update_bullets()
            self.update_world()
        except Exception as e:
            exc_type, exc_obj, exc_tb = exc_info()
            fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            exception(f'Update Failed {exc_type} {fname} {exc_tb.tb_lineno}\t\t{exc_info()}')

    def update_world(self):
        self.world.clear()
        self.world.add(self.player.location, "P")
        for e in self.enemies:
            for c in e.get_coords():
                self.world.add(c, "E")
        for b in self.bullets:
            for c in b.get_coords():
                char = 'C'
                if b.target == 'PLAYER':
                    char = 'B'
                self.world.add(c, char)
        for d in self.debrie:
            self.world.add(d.location, "D")

    def update_ai(self):
        try:
            if self.player_ai is None:
                return

            mid = (0, 0)

            if self.target is None or self.target.location[0] < self.player_ai.location[0]:
                self.target = get_closest_actor(self.player_ai.location, self.enemies)
            if self.target is None:
                return
            exception(self.target.location)
            mid = get_model_heart_location(self.target.model)
            
            for x in range(self.player_ai.location[0], self.player_ai.location[0] + 25):
                for y in range(self.player_ai.location[1] - 5, self.player_ai.location[1] + 5):
                    if 'C' in self.world.get((x, y)):
                        self.player_ai.moveby(0, max( -1, min(self.player_ai.location[1] - y, 1) ) )

            if not self.target.alive:
                self.target = None
                return

            t = self.target.location
            target_loc = (t[0] + mid[0], t[1] + mid[1])
            player_loc = self.player_ai.location

            if target_loc[1] > player_loc[1] and player_loc[1] < misc.SCREEN_HEIGHT:
                self.player.moveby(0, 1)
                return
            elif target_loc[1] < player_loc[1] and player_loc[1] > 0:
                self.player.moveby(0, -1)
                return

            gun_locations = self.player_ai.get_guns()
            for location in gun_locations:
                self.spawnBullet(location)
        except Exception as e:
            exc_type, exc_obj, exc_tb = exc_info()
            fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            exception(f'Update Failed {exc_type} {fname} {exc_tb.tb_lineno}\t\t{exc_info()}')
    
    def update_player(self):
        self.player.update()
        for c in self.player.get_coords():
            record = self.world.get(c)
            if record is None:
                continue
            if 'C' in record:
                self.player.take_damage(damage=10, damage_only=True)

    def update_enemies(self):
        NUM_ENEMIES = 35
        speed = 10
        shoot_chance = 1.5

         # Spawn enemies
        if len(self.enemies) < NUM_ENEMIES:
            x = randint(75, misc.SCREEN_WIDTH + 150)
            # y = self.player.location[1] + randint(-75, 75)
            # y = min(max(10, y), misc.SCREEN_HEIGHT - 30)

            # x = misc.SCREEN_WIDTH / 2
            y = randint(-10, misc.SCREEN_HEIGHT - 10)
            self.spawnEnemy((x, y))

        for enemy in self.enemies:
            enemy.move()
            enemy.update()
            # Kill enemies
            if enemy.alive is False:
                middle = enemy.middle
                for y in range(0, len(enemy.model)):
                    for x in range(0, len(enemy.model[y])):
                        if not enemy.model[y][x] == ' ':
                            velx = (x - middle[0]) * 25
                            vely = (y - middle[1]) * 25
                            self.spawnDebris( 
                                (
                                    enemy.location[0] + x, 
                                    enemy.location[1] + y 
                                ),
                                enemy.model[y][x],
                                5 if velx == 0 else velx,
                                5 if vely == 0 else vely,
                                True
                            )
                self.player.score += 1#enemy.score
                self.enemies.remove(enemy)

            if enemy.location[0] < -150:
                self.enemies.remove(enemy)
                self.player.score -= 10

            # Check Collision Enemy
            for c in enemy.get_coords():
                record = self.world.get(c)
                if record is None:
                    continue
                if 'B' in record:
                    damage = enemy.take_damage(location=c, damage=1)
                    if damage is not None:
                        char, loc = damage
                        if char != 'H':
                            enemy.alive = False
                        velx = max(abs(loc[0] - enemy.middle[0]) * 15, 15)
                        vely = (loc[1] - enemy.middle[1]) * 15
                        self.spawnDebris(c, icon=char, xv=velx , yv=vely, special=True)
                        self.player.score += 10

            # Move enemies
            enemy.move()

            # Enemy Shoot
            if randint(1, 5000) == shoot_chance:
                coords = enemy.get_guns()
                for coord in coords:
                    self.spawnBullet(coord, target='ENEMY')

    def update_bullets(self):
        for bullet in self.bullets:
            if bullet.alive is False:
                self.bullets.remove(bullet)
            # Move bullets
            bullet.move()
            if self.isoutofbounds(bullet.location):
                bullet.alive = False
                # self.bullets.remove(bullet)

            # Collisions
            record = self.world.get(bullet.location)
            if 'E' in record:
                if bullet.target == 'PLAYER':
                    bullet.alive = False
                    # self.bullets.remove(bullet)

    def update_debrie(self):
        self.spawn_debrie()
        for deb in self.debrie:
            deb.move()
            if self.isoutofbounds(deb.location):
                if not deb.icon == '*':
                    self.player.score += 1
                self.debrie.remove(deb)

    def spawn_debrie(self, number=50):
        if randint(0, 1000) == number:
            self.spawnDebris((
                randint(0, misc.SCREEN_WIDTH),
                randint(0, misc.SCREEN_HEIGHT)),
                icon="*",
                xv=-100,
                yv=0
            )

    def isoutofbounds(self, location):
        if location[0] < 0 or \
            location[0] > misc.SCREEN_WIDTH or \
            location[1] < 0 or \
            location[1] > misc.SCREEN_HEIGHT:
                        return True
        return False

    def explodeActor(self, actor, speed=10, icon="*"):
        self.spawnDebris(actor.location, icon=icon, xv=speed, yv=-speed)
        self.spawnDebris(actor.location, icon=icon, xv=speed, yv=0)
        self.spawnDebris(actor.location, icon=icon, xv=speed, yv=speed)
        self.spawnDebris(actor.location, icon=icon, xv=0, yv=speed)
        self.spawnDebris(actor.location, icon=icon, xv=-speed, yv=speed)
        self.spawnDebris(actor.location, icon=icon, xv=-speed, yv=-speed)
        self.spawnDebris(actor.location, icon=icon, xv=0, yv=-speed)

    def spawnDebris(self, location, icon='X', xv=0, yv=0, special=False):
        # return
        self.debrie.append(Debrie(location, icon=icon, xv=xv, yv=yv, special=special))

    def spawnBullet(self, location, target="PLAYER"):
        # if len(self.enemies) == 0:
        #     return
        # try:
        # except Exception as e:
        #     warning(str(e))
        # velocity = (100, 0)

        if target == 'PLAYER':
            b = Bullet(target, location, icon='>')
            b.setVelocity(100, 0)
        else:
            b = Bullet(target, location, icon='<')
            # velocity = look_at_velocity(self.player.location, location)
            # b.setVelocity(velocity[0], velocity[1])
            b.setVelocity(-100, 0)
        self.bullets.append(b)



    def spawnEnemy(self, location):
        models = [
            'models/enemy_small_1.mod',
            'models/enemy_small_2.mod',
            'models/enemy_small_3.mod',
            'models/enemy_small_4.mod',
            'models/enemy_small_5.mod',
            'models/enemy_med_1.mod',
            'models/enemy_med_2.mod',
            'models/enemy_med_3.mod',
            'models/enemy_med_4.mod',
            'models/enemy_big_1.mod',
            'models/enemy_big_2.mod',
            'models/enemy_big_3.mod',
            'models/enemy_big_4.mod',
            'models/enemy_big_5.mod',
            'models/enemy_big_6.mod',
            'models/enemy_big_7.mod',
            'models/enemy_big_8.mod',
            'models/enemy_large_1.mod',
            'models/enemy_large_2.mod',
            'models/enemy_large_3.mod',
            'models/enemy_large_4.mod',
        ]
        boss_models = [
            'models/enemy_boss_1.mod',
            'models/enemy_boss_2.mod',
            'models/enemy_boss_3.mod'
        ]
        try:
            spawn_key = models[randint(0, len(models) - 1)]
            if self.player.score % 100 >= 90:
                spawn_key = boss_models[randint(0, len(boss_models) - 1)]
                location=(misc.SCREEN_WIDTH, misc.SCREEN_HEIGHT / 4)
            if spawn_key in self.loaded_enemies:
                e = deepcopy(self.loaded_enemies[spawn_key])
            else:
                e = Enemy(
                    location,
                    spawn_key,
                    xv=randint(-15, -5), yv=0
                )
                self.loaded_enemies[spawn_key] = deepcopy(e)
            e.location = location
            self.enemies.append(e)
        except Exception as e:
            exception(str(e))
