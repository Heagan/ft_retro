from player import Player

from curses import flash
from time import sleep, time

class Base(Player):

    def __init__(self, location, model, lives=1, health=10000):
        Player.__init__(self, location, model)

        self.immunity_t = 0

        self.lives = lives
        self.health = health
        self.score = 0


    def take_damage(self, location=(0, 0), damage: float=1, damage_only=False):

        passed = time() - self.immunity_t
        if passed < 0.5:
            return

        super().take_damage(location, damage, damage_only)

        if self.health <= 0 or self.alive is False:
            self.lives -= 1
            self.health = 100
            self.alive = True
            flash()
        if self.lives <= 0:
            self.alive = False
            self.health = 0
            flash()
            flash()
            flash()
            sleep(2)
        self.immunity_t += passed

            

